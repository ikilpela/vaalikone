# Vaalikone - An election aid web application

A school project made in HAMK. The goal was to extend an existing voting aid application with two new functionalities: allow candidates to answer questions and allow reporters to add new questions. We also included a simple realm based authentication in our app and made a simple admin panel where admins can add new candidates and users. 
 
All code related to JSF pages (views, controllers and validators) made by Ismo. All code related to JSP pages by Mikko Uotila.