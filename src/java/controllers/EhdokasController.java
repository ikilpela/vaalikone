/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import controllers.util.JsfUtil;
import entity.Ehdokas;
import entity.User;
import facades.EhdokasFacade;
import java.io.Serializable;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

/**
 * Kontrolleri, joka tarjoaa CRUD-operaatiot ja listauksen ehdokas-olioille.
 * @author Ismo
 */
@ManagedBean
@ViewScoped
public class EhdokasController implements Serializable {

    @EJB
    private EhdokasFacade facade;

    private List<Ehdokas> ehdokkaat;
    private Ehdokas selectedEhdokas;

    private final static String EDIT_HEADER = "Muokkaa ehdokkaan tietoja";
    private final static String NEW_HEADER = "Luo uusi ehdokas";

    /**
     * Initialisaatiometodi. Hakee kaikki ehdokkaat listaan ja asettaa oletuksena valituksi ehdokkaaksi kirjautuneen ehdokkaan.
     * 
     */
    @PostConstruct
    public void init() {
        ehdokkaat = facade.findAll();
        Map map = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
        User user = (User) map.get("User");
        if (user != null) {
            selectedEhdokas = user.getEhdokas();
        }
    }

    /**
     * Palauttaa listan ehdokkaista
     * @return Lista ehdokkaista
     */
    public List<Ehdokas> getEhdokkaat() {
        return ehdokkaat;
    }

    /**
     * Asettaa ehdokkaat
     * @param ehdokkaat 
     */
    public void setEhdokkaat(List<Ehdokas> ehdokkaat) {
        this.ehdokkaat = ehdokkaat;
    }

    /**
     * Palauttaa kontrollerissa valitun ehdokkaan
     * @return Valittu ehdokas
     */
    public Ehdokas getSelectedEhdokas() {
        return selectedEhdokas;
    }

    /**
     * Asettaa valitun ehdokkaan
     * @param selectedEhdokas Ehdokas, joka halutaan valita
     */
    public void setSelectedEhdokas(Ehdokas selectedEhdokas) {
        this.selectedEhdokas = selectedEhdokas;
    }

    /**
     * Päivittää olemassa olevan ehdokkaan tiedot tai luo uuden ehdokkaan, 
     * mikäli ehdokas ei ole vielä tietokannassa.
     */
    public void updateSelected() {
        if (selectedEhdokas != null) {
            if (selectedEhdokas.getEhdokasId() != null) {
                facade.edit(selectedEhdokas);
                JsfUtil.addSuccessMessage("Ehdokas päivitettiin onnistuneesti");
            } else {
                facade.create(selectedEhdokas);
                ehdokkaat.add(selectedEhdokas);
                JsfUtil.addSuccessMessage("Uusi ehdokas tallenneettiin onnistuneesti");
            }
        }
    }

    /**
     * Poistaa valitun ehdokkaan tietokannasta ja kontrollerista.
     */
    public void removeSelected() {
        ehdokkaat.remove(selectedEhdokas);
        facade.remove(selectedEhdokas);
        JsfUtil.addSuccessMessage("Ehdokas poistettiin onnistuneesti");
    }

    /**
     * Asettaa uuden (tyhjän) ehdokkaan valituksi ehdokkaaksi
     */
    public void createNew() {
        selectedEhdokas = new Ehdokas();
    }

    /**
     * Palauttaa otsikon riippuen onko kontrollerissa valittuna uusi ehdokas
     * vai jo olemassa oleva. 
     * @return "Muokkaa ehdokkaan tietoja", jos ehdokas on olemassa, tai 
     * "Luo uusi ehdokas", jos ehdokasta ei vielä ole tietokannassa.
     */
    public String getHeaderString() {
        if (selectedEhdokas != null) {
            if (selectedEhdokas.getEhdokasId() != null) {
                return EDIT_HEADER;
            } else {
                return NEW_HEADER;
            }
        }
        return "";
    }
}
