/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import controllers.util.JsfUtil;
import entity.Kysymys;
import facades.KysymysFacade;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import org.primefaces.event.RowEditEvent;

/**
 * Kontrolleri, joka tarjoaa CRUD-operaatiot ja listauksen kysymyksille
 *
 * @author Ismo-Laptop
 */
@ViewScoped
@Named("kysymysController")
public class KysymysController implements Serializable {

    @EJB
    private KysymysFacade facade;
    private List<Kysymys> kysymykset;
    private String newKysymys;

    /**
     * Initialisaatiometodi, joka hakee tietokannasta kysymykset listaan.
     */
    @PostConstruct
    private void init() {
        this.kysymykset = facade.findAll();
    }

    public void addNew() {
        Kysymys k = new Kysymys();
        kysymykset.add(k);
    }

    /**
     * Tallentaa uuden kysymyksen tietokantaan tai muokkaa olemassa olevan
     * kysymyksen tiedot.
     *
     * @param k Kysymys, joka halutaan tallentaa.
     */
    public void createOrEdit(Kysymys k) {
        if (k.getKysymysId() != null) {
            facade.edit(k);
            JsfUtil.addSuccessMessage("Kysymys päivitettiin onnistuneesti");
        } else {
            facade.create(k);
            JsfUtil.addSuccessMessage("Uusi kysymys luotiin onnistuneesti");
        }
    }

    /**
     * Tapahtumakuuntelija, jota kutsutaan kun rivin editointi hyväksytään.
     */
    public void updateEdited(RowEditEvent event) {
        Kysymys k = (Kysymys) event.getObject();
        createOrEdit(k);
    }

    /**
     * Tapahtumankuuntelija, jota kutsutaan, kun rivin editointi perutaan.
     */
    public void onEditCancel(RowEditEvent event) {
        JsfUtil.addSuccessMessage("Muokkaus peruttiin.");
    }

    /**
     * Poistaa annetun kysymyksen tietokannasta ja kontrollerista.
     */
    public void removeKysymys(Kysymys k) {
        kysymykset.remove(k);
        facade.remove(k);
    }

    public List<Kysymys> getKysymykset() {
        return kysymykset;
    }

    public void setKysymykset(List<Kysymys> kysymykset) {
        this.kysymykset = kysymykset;
    }

    public String getNewKysymys() {
        return newKysymys;
    }

    public void setNewKysymys(String newKysymys) {
        this.newKysymys = newKysymys;
    }

}
