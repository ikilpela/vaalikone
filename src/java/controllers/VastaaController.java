/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import entity.Ehdokas;
import entity.Kysymys;
import entity.User;
import entity.Vastaus;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import org.primefaces.context.RequestContext;

/**
 *
 * @author Ismo
 */
@SessionScoped
@ManagedBean(name = "vastaaController")
public class VastaaController implements Serializable {

    private Logger logger = Logger.getLogger(getClass().getName());
    private Ehdokas ehdokas;

    private List<Kysymys> kysymykset;
    private Vastaus[] vastaukset;
    
    private int vastattuCount;
    private int currentIndex;

    /**
     * Vastauksen numeroarvoja vastaavat tekstit. 
     */
    private final String[] kuvaukset = new String[]{
        "",
        "Täysin eri mieltä",
        "Jokseenkin eri mieltä",
        "En osaa sanoa",
        "Jokseenkin samaa mieltä",
        "Täysin samaa mieltä"};

    @EJB
    private facades.KysymysFacade kysymysFacade;

    @EJB
    private facades.VastausFacade vastausFacade;

    /**
     * Valmistelee kontrollerin alustamalla kysymykset, vastaukset ja 
     * ehdokkaan tiedot
     */
    @PostConstruct
    private void prepareModel() {

        // Haetaan käyttäjä sessiosta
        FacesContext c = FacesContext.getCurrentInstance();
        Map map = c.getExternalContext().getSessionMap();
        User user = (User) map.get("User");
        ehdokas = user.getEhdokas();

        // Haetaan kysymykset ja alustetaan saman kokoinen taulu vastauksille
        kysymykset = kysymysFacade.findAll();
        vastaukset = new Vastaus[kysymykset.size()];

        currentIndex = 0;

        // Haetaan jo olemassa olevat vastaukset tilapäiseen listaan.
        List<Vastaus> tempVastaukset = vastausFacade.findByEhdokas(ehdokas.getEhdokasId());

        // Käydään kysymykset läpi ja asetetaan samalle indeksille vastaustauluun 
        // tietokannasta luettu vastaus tai uusi vastaus-olio, 
        // joka samalla myös tallennetaan tietokantaan.
        for (int i = 0; i < kysymykset.size(); i++) {
            int kysymysId = kysymykset.get(i).getKysymysId();
            for (Vastaus v : tempVastaukset) {
                if (v.getVastausPK().getKysymysId() == kysymysId) {
                    vastaukset[i] = v;
                }
            }
            if (vastaukset[i] == null) {
                vastaukset[i] = new Vastaus(ehdokas.getEhdokasId(), kysymysId);
                vastausFacade.create(vastaukset[i]);
            }
        }
    }

    /**
     * Palauttaa arvon onko kyseisellä indeksillä olevaan kysymykseen vastattu
     *
     * @param index Haettu indeksi
     * @return True, jos indeksin kysymykseen on vastattu
     */
    public boolean isAnswered(int index) {
        return (vastaukset[index].getVastaus() != null);
    }

    /**
     * Tallentaa vastauksen
     *
     * @return Sivu, jolla navigoidaan tallennuksen tuloksena.
     */
    public String save() {
        vastausFacade.edit(getCurrentVastaus());
        return "vastaa";
    }

    /**
     * Siirtyy seuraavaan kysymykseen ja samalla päivittää vastauksen
     * tietokantaan
     *
     * @return Sivu, jolle navigoidaan toiminnon tuloksena
     */
    public String next() {
        vastausFacade.edit(getCurrentVastaus());
        currentIndex++;
        return "vastaa";
    }

    /**
     * Siirtyy edelliseen kysymykseen ja samalla päivittää vastauksen
     * tietokantaan
     *
     * @return Sivu, jolle navigoidaan toiminnon tuloksena
     */
    public String previous() {
        vastausFacade.edit(getCurrentVastaus());
        currentIndex--;
        return "vastaa";
    }

    /**
     * Siirtyy indeksin mukaiseen kysymykseen ja samalla päivittää vastauksen
     * tietokantaan
     *
     * @return Sivu, jolle navigoidaan toiminnon tuloksena
     */
    public String setIndex(int index) {
        vastausFacade.edit(getCurrentVastaus());
        currentIndex = index;
        return "vastaa";
    }

    /**
     * Palauttaa tämänhetkistä indeksia vastaavan kysymyksen.
     *
     * @return Tämän hetkinen kysymys
     */
    public Kysymys getCurrent() {
        return kysymykset.get(currentIndex);
    }

    /**
     * Palauttaa tämänhetkistä indeksia vastaavan vastauksen
     *
     * @return Tämän hetkinen vastaus
     */
    public Vastaus getCurrentVastaus() {
        return vastaukset[currentIndex];
    }

    /**
     * Palauttaa vastausvaihtoehtojen (1-5) kuvaukset.
     * 
     * @return Taulukko, jossa kuvaukset eri vastausvaihtoehdoille.
     */
    public String[] getKuvaukset() {
        return kuvaukset;
    }

    /**
     * Palauttaa taulukossa vastaukset kaikkiin kysymyksiin
     * 
     * @return Taulukko vastauksia
     */
    public Vastaus[] getVastaukset() {
        return vastaukset;
    }

    
    /**
     * Palauttaa aktiivisen ehdokkaan
     * 
     * @return Ehdokas
     */
    public Ehdokas getEhdokas() {
        return ehdokas;
    }

    /**
     * Palauttaa kysymykselle otsikon muodossa "Kysymys 1/19"
     * 
     * @return Otsikko stringinä
     */
    public String getTitle() {
        return "Kysymys " + (currentIndex + 1) + " / " + getCount();
    }

    /**
     * Palauttaa kysymysten määrän
     * @return kysymysten määrä
     * 
     */
    public int getCount() {
        return kysymykset.size();
    }

    /**
     * Palauttaa aktiivisen indeksin
     * @return Aktiivinen indeksi
     */
    public int getCurrentIndex() {
        return currentIndex;
    }

    /**
     * Palauttaa kysymykset listana
     * @return Lista kysymyksiä
     */
    public List<Kysymys> getKysymykset() {
        return kysymykset;
    }
    
    public void finish() {
        for (int i = 0; i < getCount(); i++) {
            if (!isAnswered(i)) {
                RequestContext.getCurrentInstance().execute("PF('warn-dialog').show()");
                return;
            }
        }
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect("lopetus.xhtml");
        } catch (IOException ex) {
            Logger.getLogger(VastaaController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
