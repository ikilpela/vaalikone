package controllers;

import entity.User;
import facades.UserFacade;
import java.io.Serializable;
import java.security.Principal;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * Yksinkertainen controlleri, jonka avulla palveluun voidaan kirjautua sisään
 * ja ulos.
 *
 * @author Ismo
 */
@ManagedBean
@SessionScoped
public class LoginController implements Serializable {

    private static final long serialVersionUID = 3254181235309041386L;

    private static Logger log = Logger.getLogger(LoginController.class.getName());

    @EJB
    private UserFacade userEJB;

    private String email;
    private String password;
    private User user;

    /**
     * Yrittää kirjata käyttäjän sisään palveluun LoginControllerin sen
     * hetkisellä sähköpostiosoitteella ja salasanalla.
     *
     * @return Sivun nimi, jolle navigoidaan kirjautumisen tuloksena.
     */
    public String login() {
        FacesContext context = FacesContext.getCurrentInstance();
        ExternalContext externalContext = context.getExternalContext();
        HttpServletRequest request = (HttpServletRequest) externalContext.getRequest();

        // Yritetään kirjautua sisään
        try {
            request.login(email, password);
            // Jos kirjautuminen onnistui, tallennetaan käyttäjä sessioon
            Principal principal = request.getUserPrincipal();
            this.user = userEJB.find(principal.getName());
            Map<String, Object> sessionMap = externalContext.getSessionMap();
            sessionMap.put("User", user);

            // Laitetaan login-succesful viesti FacesMessage-palveluun käyttäen setKeepMessagesia, 
            // jotta ilmoitus näkyy myös uudella sivulla.
            externalContext.getFlash().setKeepMessages(true);
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Kirjauduttiin sisään onnistuneesti!", null));

        } catch (ServletException e) {
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Kirjautuminen epäonnistui", "Tarkista sähköpostiosoite ja salasana"));
            return "index";
        }
        // Jos käyttäjä on ehdokas, ohjataan suoraan vastaa-sivulle
        if (request.isUserInRole("ehdokas")) {
            return "/vastaa?faces-redirect=true";
            // Jos käyttäjä on admin, ohjataan suoraan admin sivulle
        } else if (request.isUserInRole("admin")) {
            return "/admin/index.xhtml?faces-redirect=true";
            // Jos käyttäjä on toimittaja, ohjataan kysymykset sivulle
        } else if (request.isUserInRole("toimittaja")) {
            return "/Kysymykset?faces-redirect=true";
        }
        return "/index?faces-redirect=true";
    }

    /**
     * Kirjaa kirjautuneen käyttäjän ulos palvelusta.
     *
     * @return Sivun nimi, jolle navigoidaan uloskirjautumisen jälkeen.
     */
    public String logout() {
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
        context.getExternalContext().getFlash().setKeepMessages(true);

        // Yritetään kirjautua ulos ja näytetään tulos FacesMessagella
        try {
            this.user = null;
            request.logout();
            // Tyhjennetään sessio
            ((HttpSession) context.getExternalContext().getSession(false)).invalidate();
            
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Kirjauduttiin ulos onnistuneesti!", null));
        } catch (ServletException e) {
            log.log(Level.SEVERE, "Uloskirjautuminen epäonnistui!", e);
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Uloskirjautuminen epäonnistui!", "Mikäli ongelma toistuu, ota yhteyttä ylläpitoon."));
        }
        // Navigoidaan käyttäjä takaisin etusivulle
        return "/index?faces-redirect=true";
    }

    /**
     * Palauttaa sisään kirjatun käyttäjän.
     *
     * @return Sisään kirjautunut käyttäjä.
     */
    public User getAuthenticatedUser() {
        return user;
    }

    /**
     * Palauttaa kirjautumiseen käytettävän sähköpostiosoitteen arvon.
     *
     * @return Sähköpostiosoite.
     */
    public String getEmail() {
        return email;
    }

    /**
     * Asettaa kirjautumiseen käytettävän e-mail tunnuksen.
     *
     * @param email Kirjautumiseen käytettävä sähköpostiosoite.
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Palauttaa kirjautumiseen käytettävän salasanan.
     *
     * @return salasana
     */
    public String getPassword() {
        return password;
    }

    /**
     * Asettaa kirjautumiseen käytettävän salasanan.
     *
     * @param password Salasana.
     */
    public void setPassword(String password) {
        this.password = password;
    }
}
