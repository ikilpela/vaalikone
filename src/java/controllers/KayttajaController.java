/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import controllers.util.JsfUtil;
import entity.Ehdokas;
import entity.User;
import facades.EhdokasFacade;
import facades.UserFacade;
import java.io.Serializable;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.primefaces.context.RequestContext;
import org.primefaces.event.RowEditEvent;

/**
 * Kontrolleri, joka tarjaa CRUD-operaatiot ja listauksen käyttäjille.
 *
 * @author Ismo
 */
@ManagedBean
@ViewScoped
public class KayttajaController implements Serializable {

    Map<String, String> groupNames;

    private static final Logger LOG = Logger.getLogger(KayttajaController.class.getName());

    @EJB
    private UserFacade facade;

    @EJB
    private EhdokasFacade ehdokasFacade;

    private List<User> kayttajat;
    private List<Ehdokas> ehdokasList;

    private Map<String, Integer> ehdokkaat;

    private String uusiEmail;
    private String uusiSalasana;

    private User selectedUser;

    /**
     * Alustaa kontrollerin tiedot.
     */
    @PostConstruct
    public void init() {
        kayttajat = facade.findAll();
        groupNames = new HashMap<>();
        groupNames.put("Admin", "admin");
        groupNames.put("Ehdokas", "ehdokas");
        groupNames.put("Toimittaja", "toimittaja");
        ehdokkaat = new HashMap<>();
        ehdokkaat.put("Ei ehdokasta", null);
        ehdokasList = ehdokasFacade.findAll();
        for (Ehdokas e : ehdokasList) {
            ehdokkaat.put(e.getEtunimi() + " " + e.getSukunimi(), e.getEhdokasId());
        }
    }

    /**
     * Luo uuden käyttäjän uusiSalasana ja uusiEmail kenttiin tallennettujen
     * tietojen pohjalta.
     */
    public void createNew() {
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] hash = digest.digest(uusiSalasana.getBytes(StandardCharsets.UTF_8));
            String hashString = bytesToHex(hash);
            User newUser = new User(uusiEmail, hashString);
            facade.create(newUser);
            kayttajat.add(facade.find(uusiEmail));
            JsfUtil.addSuccessMessage("Uusi käyttäjä luotiin onnistuneesti");
        } catch (Exception ex) {
            JsfUtil.addErrorMessage("Käyttäjän luominen epäonnistui");
        }
    }

    public void remove(User user) {
        try {
            facade.remove(user);
            kayttajat.remove(user);
            JsfUtil.addSuccessMessage("Käyttäjä poistettiin onnistuneesti");
        } catch (Exception ex) {
            JsfUtil.addErrorMessage("Käyttäjän poistaminen epäonnistui");
        }
    }

    /**
     * Tapahtumakuuntelija, joka tallentaa käyttäjän tiedot kun rivin editointi
     * hyväksytään.
     *
     * @param event Eventti.
     */
    public void updateEdited(RowEditEvent event) {
        User muutettu = (User) event.getObject();
        update(muutettu);
        JsfUtil.addSuccessMessage("Käyttäjän uudet tiedot tallennettiin");
    }

    /**
     * Tapahtumakuuntelija, jota kutsutaan kun rivin muokkaus perutaan.
     *
     * @param event Eventti.
     */
    public void onEditCancel(RowEditEvent event) {
        RequestContext.getCurrentInstance().update(":user-form:user-table");
        JsfUtil.addSuccessMessage("Muokkaus peruttiin");
    }

    /**
     * Päivittää parametrina annetun käyttäjän tiedot.
     * @param user Käyttäjä, jonka tiedot halutaan päivittää.
     */
    public void update(User user) {
        try {
            facade.edit(user);
            JsfUtil.addSuccessMessage("Käyttäjän tiedot päivitettiin onnistuneesti!");
        } catch (Exception e) {
            JsfUtil.addErrorMessage("Tiedon muuttaminen epäonnistui!");
        }
    }

    public String getUusiEmail() {
        return uusiEmail;
    }

    public void setUusiEmail(String uusiEmail) {
        this.uusiEmail = uusiEmail;
    }

    public String getUusiSalasana() {
        return uusiSalasana;
    }

    public void setUusiSalasana(String uusiSalasana) {
        this.uusiSalasana = uusiSalasana;
    }

    public Map<String, Integer> getEhdokkaat() {
        return ehdokkaat;
    }

    public Map<String, String> getGroupNames() {
        return groupNames;
    }

    public List<User> getKayttajat() {
        return kayttajat;
    }

    public void setKayttajat(List<User> kayttajat) {
        this.kayttajat = kayttajat;
    }

    public User getSelectedUser() {
        return selectedUser;
    }

    public void setSelectedUser(User selectedUser) {
        this.selectedUser = selectedUser;
    }

    public List<Ehdokas> getEhdokasList() {
        return ehdokasList;
    }

    public void setEhdokasList(List<Ehdokas> ehdokasList) {
        this.ehdokasList = ehdokasList;
    }

    /**
     * Returns Hex string representation of a byte array
     *
     * @param hash Bytes to convert
     * @return Hex string
     */
    private static String bytesToHex(byte[] hash) {
        StringBuilder hexString = new StringBuilder();
        for (int i = 0; i < hash.length; i++) {
            String hex = Integer.toHexString(0xff & hash[i]);
            if (hex.length() == 1) {
                hexString.append('0');
            }
            hexString.append(hex);
        }
        return hexString.toString();
    }

    /**
     * Apumetodi, jolla voidaan hakea controllerin ehdokaslistasta ehdokas ID:llä.
     * @param ehdokasId Ehdokkaan id
     * @return Ehdokas tai null, jos ID:tä vastaavaa ehdokasta ei löydetty.
     */
    public Object getEhdokas(Integer ehdokasId) {
        for (Ehdokas e : ehdokasList) {
            if (e.getEhdokasId().equals(ehdokasId)) {
                return e;
            }
        }
        return null;
    }
}
