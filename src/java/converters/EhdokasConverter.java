/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package converters;

import controllers.KayttajaController;
import entity.Ehdokas;
import javax.el.ValueExpression;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

/**
 * Konvertteri ehdokas-olioille.
 * @author Ismo
 */
@FacesConverter(value="ehdokasConverter")
public class EhdokasConverter implements Converter {

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        // Haetaan aktiivinen KayttajaController
        ValueExpression vex
                = context.getApplication().getExpressionFactory()
                        .createValueExpression(context.getELContext(),
                                "#{kayttajaController}", KayttajaController.class);
        KayttajaController controller = (KayttajaController) vex.getValue(context.getELContext());
        
        // Yritetään parseta value:sta id ja palautetaan sitä vastaava ehdokas tai null.
        try {
            Integer id = Integer.valueOf(value);
            return controller.getEhdokas(id);
        } catch (NumberFormatException e) {
            return null;
        }
    }

    
    @Override
    public String getAsString(FacesContext context, UIComponent component, Object ehdokas) {
        if (ehdokas != null) {
            Ehdokas e = (Ehdokas)ehdokas;
            return e.getEhdokasId().toString();
        } else {
            return " ";
        }
    }

}
