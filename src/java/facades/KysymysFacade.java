/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facades;

import entity.Kysymys;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Ismo
 */
@Stateless
public class KysymysFacade extends AbstractFacade<Kysymys> {

    @PersistenceContext(unitName = "VaalikonePU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public KysymysFacade() {
        super(Kysymys.class);
    }
    
}
