/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facades;

import entity.Vastaus;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Ismo
 */
@Stateless
public class VastausFacade extends AbstractFacade<Vastaus> {

    @PersistenceContext(unitName = "VaalikonePU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public VastausFacade() {
        super(Vastaus.class);
    }

    public List<Vastaus> findByEhdokas(int ehdokasId) {
        Query q = em.createNamedQuery("Vastaus.findByEhdokasId", Vastaus.class);
        q.setParameter("ehdokasId", ehdokasId);
        List<Vastaus> v = q.getResultList();
        return v;
    }

    public List<Vastaus> findAllByKysymysId(Integer id) {
        return em.createQuery("SELECT v FROM Vastaus v WHERE v.vastausPK.kysymysId = :id", Vastaus.class)
                .setParameter("id", id)
                .getResultList();
    }
}
