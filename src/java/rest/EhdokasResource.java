package rest;

import java.util.List;

import javax.ejb.EJB;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;

import facades.EhdokasFacade;
import entity.Ehdokas;
import javax.ws.rs.Produces;

@Path("/ehdokkaat")
public class EhdokasResource {

    @EJB
    EhdokasFacade facade;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Ehdokas> getAllEhdokkaat() {
        return facade.findAll();
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Ehdokas getEhdokasById(@PathParam("id") String id) {
        int parsedId = -1;
        try {
            parsedId = Integer.parseInt(id);
            return facade.find(parsedId);
        } catch (NumberFormatException ex) {
            ex.printStackTrace();
        }
        return null;
    }
}
