/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rest;

import java.util.List;

import javax.ejb.EJB;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;

import facades.VastausFacade;
import entity.Vastaus;
import java.util.ArrayList;
import javax.ws.rs.Produces;

@Path("/vastaukset")
public class VastausResource {

    @EJB
    VastausFacade facade;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Vastaus> getAll() {
        return facade.findAll();
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Vastaus> getVastauksetByKysymysId(@PathParam("id") String id) {
        int parsedId = -1;
        List<Vastaus> result = new ArrayList<>();
        try {
            parsedId = Integer.parseInt(id);
            result = facade.findAllByKysymysId(parsedId);
        } catch (NumberFormatException ex) {
            ex.printStackTrace();
        }
        return result;
    }
}
