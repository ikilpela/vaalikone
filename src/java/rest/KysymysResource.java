/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rest;

import java.util.List;

import javax.ejb.EJB;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;

import facades.KysymysFacade;
import entity.Kysymys;
import javax.ws.rs.Produces;

@Path("/kysymykset")
public class KysymysResource {

    @EJB
    KysymysFacade facade;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Kysymys> getAll() {
        return facade.findAll();
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Kysymys getKysymysById(@PathParam("id") String id) {
        int parsedId = -1;
        try {
            parsedId = Integer.parseInt(id);
            return facade.find(parsedId);
        } catch (NumberFormatException ex) {
            ex.printStackTrace();
        }
        return null;
    }
}
