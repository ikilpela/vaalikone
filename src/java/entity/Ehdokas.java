/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Ismo
 */
@Entity
@Table(name = "EHDOKKAAT")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Ehdokas.findAll", query = "SELECT e FROM Ehdokas e")
    , @NamedQuery(name = "Ehdokas.findByEhdokasId", query = "SELECT e FROM Ehdokas e WHERE e.ehdokasId = :ehdokasId")
    , @NamedQuery(name = "Ehdokas.findBySukunimi", query = "SELECT e FROM Ehdokas e WHERE e.sukunimi = :sukunimi")
    , @NamedQuery(name = "Ehdokas.findByEtunimi", query = "SELECT e FROM Ehdokas e WHERE e.etunimi = :etunimi")
    , @NamedQuery(name = "Ehdokas.findByPuolue", query = "SELECT e FROM Ehdokas e WHERE e.puolue = :puolue")
    , @NamedQuery(name = "Ehdokas.findByKotipaikkakunta", query = "SELECT e FROM Ehdokas e WHERE e.kotipaikkakunta = :kotipaikkakunta")
    , @NamedQuery(name = "Ehdokas.findByIka", query = "SELECT e FROM Ehdokas e WHERE e.ika = :ika")
    , @NamedQuery(name = "Ehdokas.findByMiksiEduskuntaan", query = "SELECT e FROM Ehdokas e WHERE e.miksiEduskuntaan = :miksiEduskuntaan")
    , @NamedQuery(name = "Ehdokas.findByMitaAsioitaHaluatEdistaa", query = "SELECT e FROM Ehdokas e WHERE e.mitaAsioitaHaluatEdistaa = :mitaAsioitaHaluatEdistaa")
    , @NamedQuery(name = "Ehdokas.findByAmmatti", query = "SELECT e FROM Ehdokas e WHERE e.ammatti = :ammatti")})
public class Ehdokas implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "EHDOKAS_ID")
    private Integer ehdokasId;
    @Size(max = 25)
    @Column(name = "SUKUNIMI")
    private String sukunimi;
    @Size(max = 25)
    @Column(name = "ETUNIMI")
    private String etunimi;
    @Size(max = 50)
    @Column(name = "PUOLUE")
    private String puolue;
    @Size(max = 25)
    @Column(name = "KOTIPAIKKAKUNTA")
    private String kotipaikkakunta;
    @Column(name = "IKA")
    private Integer ika;
    @Size(max = 250)
    @Column(name = "MIKSI_EDUSKUNTAAN")
    private String miksiEduskuntaan;
    @Size(max = 2000)
    @Column(name = "MITA_ASIOITA_HALUAT_EDISTAA")
    private String mitaAsioitaHaluatEdistaa;
    @Size(max = 50)
    @Column(name = "AMMATTI")
    private String ammatti;

    public Ehdokas() {
    }

    public Ehdokas(Integer ehdokasId) {
        this.ehdokasId = ehdokasId;
    }

    public Integer getEhdokasId() {
        return ehdokasId;
    }

    public void setEhdokasId(Integer ehdokasId) {
        this.ehdokasId = ehdokasId;
    }

    public String getSukunimi() {
        return sukunimi;
    }

    public void setSukunimi(String sukunimi) {
        this.sukunimi = sukunimi;
    }

    public String getEtunimi() {
        return etunimi;
    }

    public void setEtunimi(String etunimi) {
        this.etunimi = etunimi;
    }

    public String getPuolue() {
        return puolue;
    }

    public void setPuolue(String puolue) {
        this.puolue = puolue;
    }

    public String getKotipaikkakunta() {
        return kotipaikkakunta;
    }

    public void setKotipaikkakunta(String kotipaikkakunta) {
        this.kotipaikkakunta = kotipaikkakunta;
    }

    public Integer getIka() {
        return ika;
    }

    public void setIka(Integer ika) {
        this.ika = ika;
    }

    public String getMiksiEduskuntaan() {
        return miksiEduskuntaan;
    }

    public void setMiksiEduskuntaan(String miksiEduskuntaan) {
        this.miksiEduskuntaan = miksiEduskuntaan;
    }

    public String getMitaAsioitaHaluatEdistaa() {
        return mitaAsioitaHaluatEdistaa;
    }

    public void setMitaAsioitaHaluatEdistaa(String mitaAsioitaHaluatEdistaa) {
        this.mitaAsioitaHaluatEdistaa = mitaAsioitaHaluatEdistaa;
    }

    public String getAmmatti() {
        return ammatti;
    }

    public void setAmmatti(String ammatti) {
        this.ammatti = ammatti;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (ehdokasId != null ? ehdokasId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Ehdokas)) {
            return false;
        }
        Ehdokas other = (Ehdokas) object;
        if ((this.ehdokasId == null && other.ehdokasId != null) || (this.ehdokasId != null && !this.ehdokasId.equals(other.ehdokasId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Ehdokas[ ehdokasId=" + ehdokasId + " ]";
    }
}
