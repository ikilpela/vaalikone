/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Ismo
 */
@Embeddable
public class VastausPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "EHDOKAS_ID")
    private int ehdokasId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "KYSYMYS_ID")
    private int kysymysId;

    public VastausPK() {
    }

    public VastausPK(int ehdokasId, int kysymysId) {
        this.ehdokasId = ehdokasId;
        this.kysymysId = kysymysId;
    }

    public int getEhdokasId() {
        return ehdokasId;
    }

    public void setEhdokasId(int ehdokasId) {
        this.ehdokasId = ehdokasId;
    }

    public int getKysymysId() {
        return kysymysId;
    }

    public void setKysymysId(int kysymysId) {
        this.kysymysId = kysymysId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) ehdokasId;
        hash += (int) kysymysId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof VastausPK)) {
            return false;
        }
        VastausPK other = (VastausPK) object;
        if (this.ehdokasId != other.ehdokasId) {
            return false;
        }
        if (this.kysymysId != other.kysymysId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.VastauksetPK[ ehdokasId=" + ehdokasId + ", kysymysId=" + kysymysId + " ]";
    }
    
}
