/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Ismo
 */
@Entity
@Table(name = "VASTAUKSET")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Vastaus.findAll", query = "SELECT v FROM Vastaus v")
    , @NamedQuery(name = "Vastaus.findByEhdokasId", query = "SELECT v FROM Vastaus v WHERE v.vastausPK.ehdokasId = :ehdokasId")
    , @NamedQuery(name = "Vastaus.findByKysymysId", query = "SELECT v FROM Vastaus v WHERE v.vastausPK.kysymysId = :kysymysId")
    , @NamedQuery(name = "Vastaus.findByVastaus", query = "SELECT v FROM Vastaus v WHERE v.vastaus = :vastaus")
    , @NamedQuery(name = "Vastaus.findByKommentti", query = "SELECT v FROM Vastaus v WHERE v.kommentti = :kommentti")})
public class Vastaus implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected VastausPK vastausPK;
    @Column(name = "VASTAUS")
    private Integer vastaus;
    @Size(max = 50)
    @Column(name = "KOMMENTTI")
    private String kommentti;

    public Vastaus() {
    }

    public Vastaus(VastausPK vastausPK) {
        this.vastausPK = vastausPK;
    }

    public Vastaus(int ehdokasId, int kysymysId) {
        this.vastausPK = new VastausPK(ehdokasId, kysymysId);
    }

    public VastausPK getVastausPK() {
        return vastausPK;
    }

    public void setVastausPK(VastausPK vastausPK) {
        this.vastausPK = vastausPK;
    }

    public Integer getVastaus() {
        return vastaus;
    }

    public void setVastaus(Integer vastaus) {
        this.vastaus = vastaus;
    }

    public String getKommentti() {
        return kommentti;
    }

    public void setKommentti(String kommentti) {
        this.kommentti = kommentti;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (vastausPK != null ? vastausPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Vastaus)) {
            return false;
        }
        Vastaus other = (Vastaus) object;
        if ((this.vastausPK == null && other.vastausPK != null) || (this.vastausPK != null && !this.vastausPK.equals(other.vastausPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Vastaus[ vastausPK=" + vastausPK + " ]";
    }
    
}
