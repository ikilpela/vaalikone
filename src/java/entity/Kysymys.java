/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Ismo
 */
@Entity
@Table(name = "KYSYMYKSET")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Kysymys.findAll", query = "SELECT k FROM Kysymys k")
    , @NamedQuery(name = "Kysymys.findByKysymysId", query = "SELECT k FROM Kysymys k WHERE k.kysymysId = :kysymysId")
    , @NamedQuery(name = "Kysymys.findByKysymys", query = "SELECT k FROM Kysymys k WHERE k.kysymys = :kysymys")})
public class Kysymys implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "KYSYMYS_ID")
    private Integer kysymysId;
    @Size(max = 100)
    @Column(name = "KYSYMYS")
    private String kysymys;

    public Kysymys() {
    }

    public Kysymys(Integer kysymysId) {
        this.kysymysId = kysymysId;
    }

    public Integer getKysymysId() {
        return kysymysId;
    }

    public void setKysymysId(Integer kysymysId) {
        this.kysymysId = kysymysId;
    }

    public String getKysymys() {
        return kysymys;
    }

    public void setKysymys(String kysymys) {
        this.kysymys = kysymys;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (kysymysId != null ? kysymysId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Kysymys)) {
            return false;
        }
        Kysymys other = (Kysymys) object;
        if ((this.kysymysId == null && other.kysymysId != null) || (this.kysymysId != null && !this.kysymysId.equals(other.kysymysId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Kysymys[ kysymysId=" + kysymysId + " ]";
    }
    
}
