/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package validators;

import java.util.regex.Matcher;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import java.util.regex.Pattern;
import javax.faces.application.FacesMessage;

/**
 * Validaattori, jota käytetään salasanojen validointiin. Validaattori vaatii, 
 * että salasanassa on vähintään 8 merkkiä, yksi numero, pieni kirjain ja 
 * iso kirjain eikä yhtään välilyöntiä.
 * @author Ismo
 */
@FacesValidator("passwordValidator")
public class PasswordValidator implements Validator {

    // Regex pattern, jolla salasana tarkastetaan
    private static final String PATTERN = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=\\S+$).{8,}$";

    private Pattern pattern;
    private Matcher matcher;

    public PasswordValidator() {
        pattern = Pattern.compile(PATTERN);
    }

    @Override
    public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
//        log("Validating submitted email -- " + value.toString());
        matcher = pattern.matcher(value.toString());

        if (!matcher.matches()) {
            FacesMessage msg
                    = new FacesMessage("Salasana ei kelpaa.",
                            "Salasanan tulee vähintään 8 merkkiä pitkä ja sisältää vähintään yksi pieni kirjain, iso kirjain ja numero");
            msg.setSeverity(FacesMessage.SEVERITY_ERROR);
            throw new ValidatorException(msg);
        }
    }
}
