<%-- 
    Document   : kyselylomake
    Created on : 27.4.2018, 12:40:43
    Author     : T440
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>

        <link rel="stylesheet" type="text/css" href="resources/css/tyylit.css">
        <link rel="stylesheet" type="text/css" href="resources/css/style.css">
    </head>
    <body>
        <div class="wrap">
            <nav>
                <div class="title"><h1>Kysymykset</h1></div>
                <!--<div class="login"><ui:include src="login.xhtml"></ui:include></div>-->
            </nav>
            <!--<h:messages errorClass="message-error" infoClass="message-info" warnClass="message-warn" fatalClass="message-fatal" layout="list"/>-->
            <!--<p:growl id="main-messages" autoUpdate="true" showDetail="true" sticky="true"></p:growl>-->
            <div class='content' style="margin: 10px;">

                <h2>Kysymyksen lisäys vaalikoneeseen</h2>
                <form name="myForm" action="LueJaTallennaKysymys" method="post" onsubmit="return validateForm()">
                    <input type="text" name="kysymys" value="" style="width: 500px;"><br>
                    <br>
                    <input type= 'submit' name='ok' value='Lisää kysymys' class="button button2">          
                </form>
                <br>

                <script type="text/javascript">
                    function validateForm() {
                        var x = document.forms["myForm"]["kysymys"].value;
                        if (x == "") {
                            alert("Kysymyskenttä on tyhjä!");
                            return false;
                        }
                    }
                </script>

                <c:if test="${question != null}">
                    Kysymys <b>${question.kysymys}</b> lisättiin tietokantaan.
                    <br>
                </c:if>

                <br>
                <table>
                    <tr>
                        <th>Id</th>
                        <th>Kysymys</th>
                    </tr>
                    <c:forEach items="${requestScope.kysymykset}" var="kysymys">
                        <tr>
                            <td>${kysymys.kysymysId}</td>                  
                            <td>${kysymys.kysymys}</td>
                        </tr>

                    </c:forEach>
                </table>  
                <br>

                <a href="/Vaalikone/index.xhtml">Takaisin etusivulle</a>
            </div>
        </div>

    </body>
</html>
